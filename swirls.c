#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <SDL2/SDL.h>
#include <math.h>

int main () 
{
	SDL_Init(SDL_INIT_VIDEO);

    SDL_Window *window = SDL_CreateWindow(
        "An SDL2 window",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        500, 500,
        SDL_WINDOW_FULLSCREEN_DESKTOP
    );

    // Check that the window was successfully created
    if (window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == NULL){
        printf("Could not create renderer: %s\n", SDL_GetError());
        SDL_Quit();
        return 1;
    }

    int client_width, client_height;
    SDL_GetWindowSize(window, 
        &client_width, &client_height);

    SDL_Texture *texture = SDL_CreateTexture(renderer, 
        SDL_PIXELFORMAT_ARGB8888, 
        SDL_TEXTUREACCESS_STREAMING, 
        client_width, client_height);

    int bytes_per_pixel = 4;

    void *pixels = mmap(0, client_width * client_height * bytes_per_pixel,
        PROT_READ | PROT_WRITE,
        MAP_ANON | MAP_PRIVATE,
        -1, 0);

    int texture_pitch = client_width * bytes_per_pixel;

    int offset = 0;
    int offset_2 = 0;

    // Game loop begins here
    bool quit = false;

    int loop_index = 0;
    int loop_count = 20;

	while(!quit)
	{
	    SDL_Event event;
	    while(SDL_PollEvent(&event))
        {
            switch(event.type) 
            {
                case SDL_QUIT:
                {
                    quit = true;
                } break;

                case SDL_KEYDOWN: 
                {
                    switch(event.key.keysym.sym)
                    {
                        case SDLK_LEFT:
                        {
                            --loop_index;
                            offset = 0;
                            offset_2 = 0;
                            offset = 0;
                        } break;

                        case SDLK_RIGHT:
                        {
                            ++loop_index;
                            offset = 0;
                            offset_2 = 0;
                            offset = 0;
                        } break;
                    }
                } break;
            }
        }

        if (loop_index > loop_count) loop_index = 0;
        if (loop_index < 0) loop_index = loop_count;

        // Main 'render' loop
        uint8_t *row = (uint8_t *)pixels;

        for (int y = 0; y < client_height; ++y)
        {
            uint32_t *pixel = (uint32_t *) row;
            for (int x = 0; x < client_width; ++x)
            {
                switch(loop_index)
                {
                    case 0:
                    {
                        // First discovery
                        *pixel++ = (uint32_t)(x * y * offset);
                    } break;

                    case 1:
                    {
                        // Original blue shell
                        *pixel++ = (uint32_t)(offset_2) % ((x * y) + 1 );
                    } break;

                    case 2:
                    {
                        // Astral blue shell into the ether
                        *pixel++ = (uint32_t)((x * y) + 1 + (offset_2 * offset)) % ((x * y) + 1); 
                    } break;

                    case 3:
                    {
                        // Horizon sweep
                        *pixel++ = (uint32_t)((offset_2) + ((x + 1) * (offset + 1) / (y + 1) * (offset + 1)) + offset);
                    } break;
                    
                    case 4:
                    {
                        // HS 2
                        *pixel++ = (uint32_t)(((y + 1) * (offset + 1) / (x + 1) * (offset + 1)) + offset);
                    } break;
                    
                    case 5:
                    {
                        // Split sweep
                        *pixel++ = (uint32_t)((offset_2) + ((x + y) * (offset + offset_2) / ((x + 1) * (offset + 1)) + y));
                    } break;
                    
                    case 6:
                    {
                        // NBP
                        *pixel++ = (uint8_t)((x / 1 + x) + offset) ^ (uint8_t)((y / 1 + y) + (offset / 10));
                    } break;
                    
                    case 7:
                    {
                        // Rhomblue (use & bit operator for dark)
                        *pixel++ = (uint8_t)((x % 1 + y) + offset) | (uint8_t)((y % 1 + x) + offset);
                    } break;

                    case 8:
                    {
                        float ty = 2.0f * M_PI * (float)y / (float)client_height;
                        float tx = 2.0f * M_PI * (float)x / (float)client_width;
                        float to = (float)offset / (float)(2.0f * M_PI);
                        if (offset > 2.0f * M_PI)
                            offset = -2.0f * M_PI;

                        *pixel++ = (float)(cosf(to) * sin(ty) + offset + tan(tx));
                    } break;

                    case 9:
                    {
                        float ty = 2.0f * M_PI * (float)y / (float)client_height;
                        float tx = 2.0f * M_PI * (float)x / (float)client_width;
                        *pixel++ = (float)(sinf(ty) * offset * sinf(tx));
                    } break;

                    case 10:
                    {
                        // Wavey sin
                        float ty = 2.0f * M_PI * (float)y / (float)client_height;
                        float tx = 2.0f * M_PI * (float)x / (float)client_width;
                        *pixel++ = (float)(sinf(ty) + (offset  * y) + (sinf(tx) * offset_2));
                    } break;

                    case 11:
                    {
                        float to = (float)offset / (float)(2.0f * M_PI);
                        if (offset_2 > 2.0f * M_PI)
                            offset_2 = -2.0f * M_PI;

                        *pixel++ = (float)(sinf(to) * x * y);
                    } break;

                    case -1:
                    {
                        // double ty = M_PI * (double)y / (double)client_height;
                        // double tx = M_PI * (double)x / (double)client_width;
                        // double to = (double)offset / (double)(M_PI);
                        // if (offset > 2.0f * M_PI)
                        //     offset = -2.0f * M_PI;

                        // double ty = M_PI * ((double)y / (double)client_height);
                        // double tx = M_PI * ((double)x / (double)client_width);
                        // if (offset > 2.0f * M_PI)
                        // {
                        //     offset -= (offset - (2.0f * M_PI));
                        // }
                        // else if (offset < -2.0f * M_PI)
                        // {
                        //     offset += (-offset - (2.0f * M_PI));
                        // }
                        // double to = (double)offset / (double)(M_PI * 2.0f);
                    } break;

                    case 12:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        *pixel++ = (uint32_t)((sin(ty) * sin(tx) * sin(tx) * (x + y) * (x + y) + (offset * offset)));
                    } break;

                    case 13:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        // Try reversing offset!
                        *pixel++ = (uint32_t)((sin(ty) + x * (offset) * sin(ty) * sin(ty) * sin(tx) * sin(tx) + y * (offset)));
                    } break;

                    case 14:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        *pixel++ = (uint32_t)((sin(ty) + x * (offset) * sin(ty) + x * (offset) * sin(ty) * sin(tx) * sin(tx) + y * (offset)));
                    } break;

                    case 15:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        *pixel++ = (uint32_t)((sin(ty) + y * (offset) * sin(ty) + y * (offset) * sin(ty) + x * (offset) * sin(tx) * sin(tx) + y * (offset)));
                    } break;

                    case 16:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        // of course
                        *pixel++ = (uint32_t)((sin(ty) + y * (offset) * sin(ty) + y * (offset) * sin(ty) + y * (offset) * sin(tx) + x * (offset) * sin(tx) + x * (offset) * sin(tx) + x * (offset)));
                    } break;

                    case 17:
                    {
                        double ty = M_PI * ((double)y / (double)client_height);
                        double tx = M_PI * ((double)x / (double)client_width);
                        // ok what? I need another lesson in trig
                        *pixel++ = (uint32_t)((cos(ty) + y * (offset) * cos(ty) + y * (offset) * cos(ty) + y * (offset) * cos(tx) + x * (offset) * cos(tx) + x * (offset) * cos(tx) + x * (offset)));
                    } break;

                    case 18:
                    {
                        *pixel++ = (uint32_t)((sin(x)*(offset + y)) * (cos(y)*(offset + x)));
                    } break;

                    case 19:
                    {
                        // Different mods, different effects
                        if ((int)(y + x) % 50)
                        {
                            *pixel++ = (uint8_t)((y + x) + (offset)) << 16 | (uint8_t)(x - (offset)) << 8 | (uint8_t)((y + x) - (offset));
                        }
                        else
                        {
                            *pixel++ = (uint8_t)(y + (offset)) << 16 | (uint8_t)(x + (offset)) << 8 | (uint8_t)(y - (offset));
                        }
                    } break;

                    case 20:
                    {
                        if ((int)(x * x + y * y) % 20)
                        {
                            *pixel++ = (uint8_t)((y + x) + (offset)) << 16 | (uint8_t)(x - (offset)) << 8 | (uint8_t)((y + x) - (offset));
                        }
                        else
                        {
                            *pixel++ = (uint8_t)(y + (offset)) << 16 | (uint8_t)(x + (offset)) << 8 | (uint8_t)(y - (offset));
                        }
                    } break;
                }

            }
            row += texture_pitch;
            ++offset_2;
        }
        ++offset;

        // Now apply pixel buffer to texture
        if(SDL_UpdateTexture(texture,
          NULL, pixels,
          texture_pitch))
        {
            printf("Could not update texture: %s\n", SDL_GetError());
        }

        // and present texture on the renderer
        SDL_RenderCopy(renderer, texture, NULL, NULL);

        SDL_RenderPresent(renderer);

	}

    // Close and destroy the window
    SDL_DestroyWindow(window);

    // Clean up
    SDL_Quit();
    return 0;

}